import {Mover} from '../mover/mover';
import {OnChanges, OnInit, Injectable} from '@angular/core';
import * as THREE from 'three';

@Injectable()
export class CameraService {

  private viewAngle: number = 75;
  private near: number = 1e2;
  private far: number = 1e15;
  private position: THREE.Vector3;

  camera: THREE.PerspectiveCamera;

  public init (aspect: number, position: THREE.Vector3) {
    this.camera = new THREE.PerspectiveCamera(
      this.viewAngle,
      aspect,
      this.near,
      this.far
    );
    this.camera.position.copy(position);
    this.camera.lookAt(new THREE.Vector3());
  }

  public follow (mover: Mover) {
    this.camera.lookAt(mover.location);
    this.camera.position.add(mover.velocity);
    // Requis apr�s modification de la position de la camera
    this.camera.updateProjectionMatrix();
  }

  updateAspect (aspect) {
    if (this.camera) {
      this.camera.aspect = aspect;
      this.camera.updateProjectionMatrix();
    }
  }
}
