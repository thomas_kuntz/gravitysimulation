import * as THREE from 'three';
import {RadialRingGeometry} from '../geometry/radialringgeometry';

export class Mover {
  index: number;
  name: string;
  emitLight: boolean;
  mass: number;
  radius: number;
  location: THREE.Vector3;
  velocity: THREE.Vector3;
  angularSpeed: number;
  obliquity: number;
  acceleration: THREE.Vector3;
  vertices: any[];
  mesh: THREE.Mesh;
  trail: THREE.Line;
  alive: boolean;
  isSelected: boolean;
  trailLength: number = 10000;
  selectionScale: number = 2e7;
  oldSpeed: number = 1;

  constructor(name: string, color: string, emitLight: boolean,
    radius: number, mass: number, loc: THREE.Vector3,
    vel: THREE.Vector3, angularSpeed: number, obliquity: number,
    textureLoader: THREE.TextureLoader) {
    this.name = name;
    this.emitLight = emitLight;
    this.radius = radius;
    this.mass = mass;
    this.location = loc;
    this.velocity = vel;
    this.angularSpeed = angularSpeed;
    this.obliquity = obliquity;

    this.acceleration = new THREE.Vector3(0, 0, 0);
    this.vertices = [];
    this.isSelected = false;

    let material: THREE.Material;
    const texture = textureLoader.load('../assets/textures/' + this.name + '_texture.jpg');
    const geometry = new THREE.SphereGeometry(this.radius, 128, 128);
    this.mesh = new THREE.Mesh();

    const selectionGeometry = new THREE.SphereGeometry(
      this.selectionScale, 64, 64);
    const selectionMaterial = new THREE.MeshBasicMaterial({
      color: color,
      transparent: true,
      opacity: 0.0
    });
    const selectionMesh = new THREE.Mesh(selectionGeometry, selectionMaterial);
    selectionMesh.name = name.concat('Selection');
    this.mesh.add(selectionMesh);

    if (this.emitLight) {
      const light = new THREE.PointLight(0xffffff, 2);
      light.name = name.concat('Light');
      // Activation des ombres
      light.castShadow = true;
      light.shadow.bias = 0.01;
      light.shadow.camera.fov = 90;
      light.shadow.camera.far = 1e15;
      light.shadow.camera.near = this.radius + 1;
      light.shadow.mapSize = new THREE.Vector2(2048, 2048);
      light.position.copy(this.location);
      light.updateMatrix();
      light.updateMatrixWorld(false);
      this.mesh.add(light);
      material = new THREE.MeshBasicMaterial({
        map: texture
      });
    } else {
      if (this.name === 'Earth') {
        const normalMap = textureLoader.load(
          '../assets/textures/' + this.name + '_normal_map.jpg');
        const specularMap = textureLoader.load(
          '../assets/textures/' + this.name + '_specular_map.jpg');
        // Generation de la Terre avec une texture am�lior�e.
        // normal map: effet de profondeur.
        // specular map: effet de reflection.
        material = new THREE.MeshPhongMaterial({
          map: texture,
          normalMap: normalMap,
          normalScale: new THREE.Vector2(2, 2),
          specularMap: specularMap
        });
      } else {
        material = new THREE.MeshPhongMaterial({
          map: texture,
          shininess: 0
        });
      }
      this.mesh.castShadow = true;
      this.mesh.receiveShadow = true;
    }

    if (this.name === 'Saturn') {
      const ringMap = textureLoader.load(
        '../assets/textures/' + this.name + '_ring_map.png');
      const ringMaterial = new THREE.MeshBasicMaterial({
        map: ringMap,
        //        shininess: 0,
        side: THREE.DoubleSide,
        vertexColors: THREE.FaceColors,
        transparent: true,
        opacity: 0.8
      });
      const ringGeometry = new RadialRingGeometry(
        74500, 140220, 128, 128, 0, 2 * Math.PI);
      const ring = new THREE.Mesh(ringGeometry, ringMaterial);
      //      ring.castShadow = true;
      //      ring.receiveShadow = true;
      this.mesh.add(ring);
    }

    this.mesh.material = material;
    this.mesh.geometry = geometry;
    this.mesh.name = name;
    this.mesh.position.copy(this.location);
    this.mesh.rotation.z = this.obliquity;

    const trailMaterial: THREE.LineBasicMaterial = new THREE.LineBasicMaterial({
      color: color,
      transparent: true,
      opacity: 0.0
    });
    const trailGeometry: THREE.Geometry = new THREE.Geometry();
    this.trail = new THREE.Line(trailGeometry, trailMaterial);
    for (let i = 0; i < this.trailLength; i++) {
      this.trail.geometry.vertices.push(this.location.clone);
    }
    this.trail.name = name.concat('Trail');
  }

  // application de l'effet de la gravit�e sur les parametres de l'objet.
  public update (speed) {
    const ratio = speed / this.oldSpeed;
    this.rotate(speed);
    this.velocity.setLength(this.velocity.length() * ratio);
    this.velocity.add(this.acceleration);
    this.oldSpeed = speed;
    this.location.add(this.velocity);
    this.acceleration.multiplyScalar(0);
    this.mesh.position.copy(this.location);
    if (this.vertices.length > 1000) {
      this.vertices.splice(0, 1);
    }
    this.vertices.push(this.location.clone());
  }

  public rotate (speed = 10) {
    this.mesh.rotateY(this.angularSpeed * speed);
  }

  // Cr�ation de la ligne d'orbite.
  public updateTrail () {
    this.trail.geometry.vertices.push(
      this.trail.geometry.vertices.shift());
    this.trail.geometry.vertices[ this.trailLength - 1 ]
      = this.location.clone();
    this.trail.geometry.verticesNeedUpdate = true;
    this.trail.geometry.computeBoundingSphere();
  }

  // Calcul de la force d'attraction.
  // F = (G * M1 * M2) / (distance �)
  public attractedBy (mover: Mover, speed) {
    let direction = new THREE.Vector3().subVectors(this.location, mover.location);
    const distance = direction.length();
    direction = direction.normalize();
    const strength = - ((6.674e-20 * this.mass * mover.mass) / (distance * distance));
    // application du ratio de vitesse.
    const force = direction.multiplyScalar(strength * speed * speed);
    this.applyForce(force);
  }

  // Calcul de l'acceleration subie.
  public applyForce (force: THREE.Vector3) {
    const f = force.divideScalar(this.mass);
    this.acceleration.add(f);
  }
}
