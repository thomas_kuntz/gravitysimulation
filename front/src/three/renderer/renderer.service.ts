import {Data} from '../../data/data';
import {DataService} from '../../data/data.service';
import {Injectable, ViewChild} from '@angular/core';
import * as THREE from 'three';
const OrbitControls = require('three-orbit-controls')(THREE);
import * as TrackballControls from 'three-trackballcontrols';
import {Mover} from '../mover/mover';
import {CameraService} from '../camera/camera.service';
import * as TWEEN from '@tweenjs/tween.js';

@Injectable()
export class RendererService {

  scene: THREE.Scene;
  renderer: THREE.WebGLRenderer;
  canvas: HTMLElement;
  width: number;
  height: number;
  speed: number = 1;
  oldSpeed: number = 1;
  controls: any;
  movers: Mover[];
  intersectable: Mover[];
  selected: Mover;
  isRunning: boolean = true;
  textureLoader: THREE.TextureLoader;
  data: any;
  frame: number;

  constructor(public cameraService: CameraService,
    private dataService: DataService) {
  }

  public init (container: HTMLElement) {
    this.renderer = new THREE.WebGLRenderer({antialias: true});
    // activation du rendu des ombres.
    this.renderer.shadowMap.enabled = true;
    // qualit� des ombres.
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    this.renderer.physicallyCorrectLights = true;
    this.canvas = this.renderer.domElement;
    // Ajout du canvas au container html.
    container.appendChild(this.canvas);
    this.width = this.canvas.clientWidth;
    this.height = this.canvas.clientHeight;
    this.renderer.setSize(this.width, this.height, false);
    const cameraPosition: THREE.Vector3 = new THREE.Vector3(-5e8, 5e8, 5e8);
    const aspect: number = this.width / this.height;
    this.cameraService.init(aspect, cameraPosition);
    // Ajout des controles de la camera.
    this.controls = new TrackballControls(this.cameraService.camera, container);
    // Ammortissement du mouvement de la camera.
    this.controls.enableDamping = true;
    this.controls.dampingFactor = 0.25;
    this.controls.enablePan = false;
    this.controls.zoomSpeed = 2;
    this.scene = new THREE.Scene();
    this.textureLoader = new THREE.TextureLoader();
  }

  public createBackground () {
    this.scene.background = new THREE.CubeTextureLoader()
      .setPath('../assets/textures/background/')
      .load([
        'right.png', 'left.png',
        'top.png', 'bottom.png',
        'front.png', 'back.png',
      ]);
  }

  public start (moverList: Data[]) {
    this.createBackground();
    this.movers = [];
    this.data = moverList;
    moverList.forEach(e => {
      this.movers.push(new Mover(e.name,
        e.color,
        e.light,
        e.radius,
        e.mass * 1e24,
        new THREE.Vector3(e.positionX, e.positionY, e.positionZ),
        new THREE.Vector3(e.velocityX, e.velocityY, e.velocityZ),
        e.angularSpeed,
        e.obliquity,
        this.textureLoader
      ));
    });
    // Cr�ation de la liste des �l�ments selectionnables.
    this.intersectable = [];
    for (let i = 0; i < this.movers.length; i++) {
      this.intersectable.push(this.movers[ i ].mesh);
      this.intersectable.push(this.movers[ i ].mesh.children[ 0 ]);
      this.scene.add(this.movers[ i ].mesh);
      this.scene.add(this.movers[ i ].trail);
    }
    //    this.selected = this.movers[ 0 ];
    //    this.controls.target = this.selected.location;
    //    this.controls.minDistance = 1.2 * this.selected.radius;
    //    this.cameraService.follow(this.selected);
  }

  public animate () {
    // Requete d'une nouvelle frame des qu'une modification de la scene est detect�e.
    window.requestAnimationFrame(_ => this.animate());
    if (this.isRunning) {
      this.render();
    } else {
      for (let i = 0; i < this.movers.length; i++) {
        this.movers[ i ].rotate();
      }
    }
    this.controls.update();
    TWEEN.update();
    this.renderer.render(this.scene, this.cameraService.camera);

  }

  public render () {
    for (let i = 0; i < this.movers.length; i++) {
      for (let j = 0; j < this.movers.length; j++) {
        if (i !== j) {
          this.movers[ i ].attractedBy(this.movers[ j ], this.speed);
        }
      }
    }
    for (let i = 0; i < this.movers.length; i++) {
      this.movers[ i ].update(this.speed);
      this.movers[ i ].updateTrail();
      if (this.movers[ i ].isSelected) {
        this.cameraService.follow(this.selected);
      }
    }
  }

  public pause () {
    this.isRunning = !this.isRunning;
    console.log('running: ' + this.isRunning);
  }

  public reset () {
    this.cleanScene();
    this.controls.reset();
    this.start(this.data);
  }

  public cleanScene () {
    for (let i = this.scene.children.length - 1; i >= 0; i--) {
      if (this.scene.children[ i ].material) {
        this.scene.children[ i ].material.dispose();
        this.scene.children[ i ].geometry.dispose();
      }
      this.scene.remove(this.scene.children[ i ]);
    }
  }



  public demo () {
    TWEEN.removeAll();
    const tweens = [];
    for (let i = this.movers.length - 1; i >= 0; i--) {
      const mover = this.movers[ i ];
      tweens.push(new TWEEN.Tween(this.cameraService.camera.position)
        .to({
          x: (mover.location.x > 0 ?
            mover.location.x - mover.radius * 1.4
            : mover.location.x + mover.radius * 1.4),
          y: mover.location.y + mover.radius * 1.1,
          z: (mover.location.z > 0 ?
            mover.location.z - mover.radius * 1.4
            : mover.location.z + mover.radius * 1.4)
        }, 30000)
        .easing(TWEEN.Easing.Quartic.InOut)
        .delay(30000)
        .onStart(() => {
          new TWEEN.Tween(this.controls.target)
            .to({
              x: '+5e5',
              z: '+5e5'
            }, 15000)
            .easing(TWEEN.Easing.Sinusoidal.InOut)
            .start()
            .delay(1000);

          new TWEEN.Tween(this.controls.target)
            .to({
              x: mover.location.x,
              y: mover.location.y,
              z: mover.location.z
            }, 20000)
            .easing(TWEEN.Easing.Sinusoidal.InOut)
            .delay(1000)
            .onStart(() => {
              this.selected = mover;
              this.cameraService.follow(this.selected);
            })
            .start();

        })
      );
    }

    for (let i = 1; i < tweens.length; i++) {
      tweens[ i - 1 ].chain(tweens[ i ]);
    }
    tweens[ 0 ].start();
    //    tweens[ tweens.length ].chain()
  }

  // Gestion de la selection par la souris.
  public onClick (event) {
    // Recuperation de la position de la souris sur le canvas
    const mouseX = 2 * (event.clientX / this.width) - 1;
    const mouseY = 1 - 2 * (event.clientY / this.height);
    const mouse = new THREE.Vector2(mouseX, mouseY);
    // Lancement d'un rayon entre la position de la camera
    // et celle de la souris.
    const raycaster = new THREE.Raycaster();
    raycaster.setFromCamera(mouse.clone(), this.cameraService.camera);
    // R�cuperation de la liste des objets rencontr�s par le rayon.
    const intersects = raycaster.intersectObjects(this.intersectable);
    if (intersects.length > 0) {
      const clickedObj = intersects[ 0 ].object;
      for (let i = 0; i < this.movers.length; i++) {
        // Deselection.
        this.movers[ i ].isSelected = false;
        // Assignement de l'�l�ment selectionn�.
        if (this.movers[ i ].mesh === clickedObj
          || this.movers[ i ].mesh.children[ 0 ] === clickedObj) {
          this.selected = this.movers[ i ];
          this.selected.isSelected = true;
          this.controls.target = this.selected.location;
          //          this.controls.minDistance = this.selected.radius + 1e3;
          this.cameraService.follow(this.selected);
        }
      }
    }
  }

  // Gestion du redimensionnement de la fen�tre.
  public onResize () {
    const width = this.canvas.clientWidth;
    const height = this.canvas.clientHeight;
    this.cameraService.camera.aspect = width / height;
    this.cameraService.camera.updateProjectionMatrix();
    this.renderer.setSize(width, height, false);
  }

}
