import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import {ThreeComponent} from './three.component';
import {DataService} from '../data/data.service';
import {CameraService} from './camera/camera.service';
import {RendererService} from './renderer/renderer.service';

@NgModule({
  declarations: [
    ThreeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [ DataService, CameraService, RendererService ],
  bootstrap: [ ThreeComponent ]
})
export class ThreeModule {}
