import {Data} from '../data/data';
import {Component, ViewChild, OnInit, ElementRef, HostListener} from '@angular/core';
import {RendererService} from './renderer/renderer.service';
import {DataService} from '../data/data.service';
import {CameraService} from './camera/camera.service';
import * as THREE from 'three';
import * as dat from 'dat.gui/build/dat.gui.js';

@Component({
  selector: 'app-root',
  template: `<div #container (window:resize)="onResize($event)" (click)="onClick($event)"></div>`
})

export class ThreeComponent implements OnInit {

  // Acces au DOM.
  @ViewChild('container') elementRef: ElementRef;
  private container: HTMLElement;
  public data: Data[];

  // Injection des services.
  constructor(private rendererService: RendererService,
    private planetService: DataService) {
  }

  ngOnInit () {
    this.container = this.elementRef.nativeElement;
    // initilisation du canvas et de la scene.
    this.rendererService.init(this.container);
    // Chargement des donn�es depuis le serveur.
    this.planetService.getData().subscribe(response => {
      this.data = [];
      response.forEach(element => {
        this.data.push(element);
      });
      this.rendererService.start(this.data);
      this.rendererService.animate();
    });
    // initialisation de l'interface GUI.
    const gui = new dat.GUI({
      width: 512,
      resizable: false
    });
    gui.add(this.rendererService, 'demo');
    gui.add(this.rendererService, 'reset');
    gui.add(this.rendererService, 'pause');
    gui.add(this.rendererService, 'speed', -10000, 10000).step(10);
  }

  // Gestion du redimensionnement de la fen�tre.
  @HostListener('window.resize', [ '$event' ]) onResize (event) {
    this.rendererService.onResize();
  }

  @HostListener('click', [ '$event' ]) onClick (event) {
    event.stopPropagation();
    this.rendererService.onClick(event);
  }
}
