/* SystemJS module definition */
declare var require: any; // remove error TS2304 cannot find name 'require'
declare var module: NodeModule;
interface NodeModule {
  id: string;
}
