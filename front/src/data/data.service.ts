import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Data} from './data';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class DataService {

  private dataUrl = '/api';

  constructor(private http: HttpClient) {
  }

  getData (): Observable<Data[]> {
    return this.http.get<Data[]>(`${ this.dataUrl }/back/rest/data`);
  }

}
