import {Vector3} from 'three';
export interface Data {
  id: number;
  name: string;
  color: string; // hexadecimal
  light: boolean;
  radius: number;
  mass: number;   // x1e24 kg
  positionX: number;
  positionY: number;
  positionZ: number;
  velocityX: number;
  velocityY: number;
  velocityZ: number;
  angularSpeed: number; // vitesse de revolution
  obliquity: number; // inclinaison de l'axe de revolution
}
