--
-- JBoss, Home of Professional Open Source
-- Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
-- contributors by the @authors tag. See the copyright.txt in the
-- distribution for a full listing of individual contributors.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- http://www.apache.org/licenses/LICENSE-2.0
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

-- Axis
--	  | y
--	  |
--    |
--	  |_______ x
--   /
--  /
-- / z

-- should rotate around y axis
-- then y = -z
-- 																																							radius, 	mass, 		posX, 				posY, 				posZ, 				velX,					velY,					velZ, 	rotation, obliquity
insert into Planet (name, color, light, radius, mass, posX, posY, posZ, velocityX, velocityY, velocityZ, angspeed, obliquity) values ('Sun', '#ffcc00', true, 696300, 1988544, 302064.7931647457, 18763.25003494404, -887813.1820159960, -0.009600037617360556, -0.000229648259696636, -0.009133885341873161, 0.0032, 0) -- sun
insert into Planet (name, color, light, radius, mass, posX, posY, posZ, velocityX, velocityY, velocityZ, angspeed, obliquity) values ('Mercury', '#8c8c8c', false, 2440, 0.3302, 53251866.95392322, 6599998.995706475, 20205911.24745180, 8.524147984700784, -3.095023210853398, -47.46400562226508, 0.00007, 0) -- mercury
insert into Planet (name, color, light, radius, mass, posX, posY, posZ, velocityX, velocityY, velocityZ, angspeed, obliquity) values ('Venus', '#ff9900', false, 6051.8, 4.8685, -88057944.18891835, -4227370.393214487, 61282059.82742291, 19.89505023430034, 1.543543509358914, 28.79746891374686, 0.0041, 177.36) -- venus
insert into Planet (name, color, light, radius, mass, posX, posY, posZ, velocityX, velocityY, velocityZ, angspeed, obliquity) values ('Moon', '#cccccc', false, 1738, 0.0734,  70651051.44361712, 14706.82149572670, -130534478.0203295, -25.90935358256262, 0.08194547411717057, -14.56482779170619, 0.00015, 6.67) -- moon
insert into Planet (name, color, light, radius, mass, posX, posY, posZ, velocityX, velocityY, velocityZ, angspeed, obliquity) values ('Earth', '#005ce6', false, 6371, 5.97219, 70439562.22612345, 24642.03829246759, -130878228.8023199, -26.71981560751822, -0.0007551611022913463, -14.03782519307870, 0.0042, 23.45) -- earth
insert into Planet (name, color, light, radius, mass, posX, posY, posZ, velocityX, velocityY, velocityZ, angspeed, obliquity) values ('Mars', '#ff1a1a', false, 3389.9, 0.64185, -246828942.7873853, -6338231.100693599, -14819835.95508911, -0.4683628369488068, 0.4520610148865165, 22.11176041710749, 0.00011, 25.19) -- mars
insert into Planet (name, color, light, radius, mass, posX, posY, posZ, velocityX, velocityY, velocityZ, angspeed, obliquity) values ('Ganymede', '#8c8c8c', false, 2634, 0.1482, -662252535.2926780, -16810276.13065717, 469476499.9143084, -3.318821345377700, 0.2031621883354293, 2.031621883354293, 0.00058, 0.33) -- ganymede
insert into Planet (name, color, light, radius, mass, posX, posY, posZ, velocityX, velocityY, velocityZ, angspeed, obliquity) values ('Europa', '#d9d9d9', false, 1565, 0.04797, -662404732.8788167, -16796877.25655976, 469870951.4156175, -6.447711161184038, 0.3695303404357180, 0.9744743464578573, 0.001173, 0.464) -- europa
insert into Planet (name, color, light, radius, mass, posX, posY, posZ, velocityX, velocityY, velocityZ, angspeed, obliquity) values ('Io', '#e6e600', false, 1821, 0.08933, -662314456.570809, -16754926.522305, 470942820.772027, 24.30391405277943, -0.2625550339629141, 5.870395017092578, 0.00024, 0.0015) -- io
insert into Planet (name, color, light, radius, mass, posX, posY, posZ, velocityX, velocityY, velocityZ, angspeed, obliquity) values ('Jupiter', '#ffcc66', false, 69911, 1898.13, -662416061.3116992, -16768062.47107017, 470535266.6378114, 7.412995756361536, 0.1241875413995288, 10.03164967633588, 0.0062, 3.13) -- jupiter
insert into Planet (name, color, light, radius, mass, posX, posY, posZ, velocityX, velocityY, velocityZ, angspeed, obliquity) values ('Saturn', '#ffcc33', false, 58232, 568.319, -22806023.15920288, -27062061.88542295, 1504241635.825181, 9.129157504889376, 0.3605652777627038, 0.1770978422992820, 0.01, 26.13) -- saturne
insert into Planet (name, color, light, radius, mass, posX, posY, posZ, velocityX, velocityY, velocityZ, angspeed, obliquity) values ('Uranus', '#00ffff', false, 25362, 86.8103, 2661637463.291363, 29516625.72005296, -1336904787.743757, -3.106476813325823, -0.06176020235499902, -5.768018229543532, 0.003, 97.77) -- uranus
insert into Planet (name, color, light, radius, mass, posX, posY, posZ, velocityX, velocityY, velocityZ, angspeed, obliquity) values ('Neptune', '#0066ff', false, 24624, 102.41, 4285674022.639176, 71945515.07913953, 1302484293.831975, 1.543804081131529, 0.1426593642928464, -5.233007023101571, 0.0028, 28.32) -- neptune
insert into Planet (name, color, light, radius, mass, posX, posY, posZ, velocityX, velocityY, velocityZ, angspeed, obliquity) values ('Pluto', '#8c8c8c', false, 1195, 0.01307, 1595119288.100681, -46040154.58834219, 4742204933.482451, 5.250721663679809, 1.585561762728450, -0.5784143123608257, 0.027, 122.53) -- pluto
