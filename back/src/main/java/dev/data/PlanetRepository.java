package dev.data;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import dev.model.Planet;

public class PlanetRepository {

	@Inject
	private EntityManager em;

	public Planet findById(Long id) {
		return em.find(Planet.class, id);
	}

	public List<Planet> findAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Planet> criteria = cb.createQuery(Planet.class);
		Root<Planet> Planet = criteria.from(Planet.class);
		criteria.select(Planet);
		return em.createQuery(criteria).getResultList();
	}

	public List<Planet> findAllOrderedByName() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Planet> criteria = cb.createQuery(Planet.class);
		Root<Planet> Planet = criteria.from(Planet.class);
		// Swap criteria statements if you would like to try out type-safe
		// criteria queries, a new
		// feature in JPA 2.0
		// criteria.select(Planet).orderBy(cb.asc(Planet.get(Planet_.name)));
		criteria.select(Planet).orderBy(cb.asc(Planet.get("name")));
		return em.createQuery(criteria).getResultList();
	}

}
