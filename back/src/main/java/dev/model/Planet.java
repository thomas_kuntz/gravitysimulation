package dev.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "PLANET")
public class Planet implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "NAME")
	@Size(min = 1, max = 25)
	private String name;

	@NotNull
	@Column(name = "COLOR")
	private String color;

	@NotNull
	@Column(name = "LIGHT")
	private boolean light;

	@NotNull
	@Column(name = "RADIUS")
	private double radius;

	@NotNull
	@Column(name = "MASS")
	private double mass;
	@NotNull
	@Column(name = "POSX")
	private double positionX;

	@NotNull
	@Column(name = "POSY")
	private double positionY;

	@NotNull
	@Column(name = "POSZ")
	private double positionZ;

	@NotNull
	@Column(name = "VELOCITYX")
	private double velocityX;

	@NotNull
	@Column(name = "VELOCITYY")
	private double velocityY;

	@NotNull
	@Column(name = "VELOCITYZ")
	private double velocityZ;

	@NotNull
	@Column(name = "ANGSPEED")
	private double angularSpeed;

	@NotNull
	@Column(name = "OBLIQUITY")
	private double obliquity;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public boolean isLight() {
		return light;
	}

	public void setLight(boolean light) {
		this.light = light;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public double getMass() {
		return mass;
	}

	public void setMass(double mass) {
		this.mass = mass;
	}

	public double getPositionX() {
		return positionX;
	}

	public void setPositionX(double positionX) {
		this.positionX = positionX;
	}

	public double getPositionY() {
		return positionY;
	}

	public void setPositionY(double positionY) {
		this.positionY = positionY;
	}

	public double getPositionZ() {
		return positionZ;
	}

	public void setPositionZ(double positionZ) {
		this.positionZ = positionZ;
	}

	public double getVelocityX() {
		return velocityX;
	}

	public void setVelocityX(double velocityX) {
		this.velocityX = velocityX;
	}

	public double getVelocityY() {
		return velocityY;
	}

	public void setVelocityY(double velocityY) {
		this.velocityY = velocityY;
	}

	public double getVelocityZ() {
		return velocityZ;
	}

	public void setVelocityZ(double velocityZ) {
		this.velocityZ = velocityZ;
	}

	public double getAngularSpeed() {
		return angularSpeed;
	}

	public void setAngularSpeed(double angularSpeed) {
		this.angularSpeed = angularSpeed;
	}

	public double getObliquity() {
		return obliquity;
	}

	public void setObliquity(double obliquity) {
		this.obliquity = obliquity;
	}
}
