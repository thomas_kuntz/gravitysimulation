package dev.service;

import java.util.List;

import javax.inject.Inject;

import dev.data.PlanetRepository;
import dev.model.Planet;

public class PlanetService {

	@Inject
	private PlanetRepository planetRepo;

	private double convertDegToRad(double deg) {
		return Math.toRadians(deg);
	}

	public List<Planet> getPlanet() {

		List<Planet> planets = planetRepo.findAll();
		for (Planet planet : planets) {
			planet.setAngularSpeed(convertDegToRad(planet.getAngularSpeed()));
			planet.setObliquity(convertDegToRad(planet.getObliquity()));
		}
		return planets;
	}
}
