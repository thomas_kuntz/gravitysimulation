package dev.rest;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dev.model.Planet;
import dev.service.PlanetService;

@Path("/data")
public class PlanetREST {

	@Inject
	private PlanetService planetService;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAll() {
		List<Planet> planets = planetService.getPlanet();
		return Response.status(200).entity(planets)
				.header("Access-Control-Allow-Origin", "*").build();
	}

}
