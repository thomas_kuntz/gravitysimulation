Gravity simulation project using N-Body.


Requirement:
 - WILDFLY 8 server
 - node.js
 - webpack server

TODO:

- UI
- Background
- Collide


DONE:

- Scene reset
- textures
- Light
- UI basic (speed slider)
- Select mover
- Physics module (N-bodies model)
- Scene
- controls
- Simple render
- Mover definition
- Physics model (1-body model)
- Trail
- Handle window resize
- Mover factory 